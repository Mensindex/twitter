package com.twitter.timur.controller;

import com.twitter.timur.forms.MessageForm;
import com.twitter.timur.models.Message;
import com.twitter.timur.models.User;
import com.twitter.timur.repositories.UserRepository;
import com.twitter.timur.security.details.UserDetailsImpl;
import com.twitter.timur.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

@Controller
public class MessagesController {
    private final MessageService messageService;

    @Autowired
    public MessagesController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/messages")
    public String getMessagesPage(Model model) {
        User user = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        List<Message> messageList = messageService.getAllMessage();
        model.addAttribute("messageList", messageList);
        model.addAttribute("user", user);
        return "messages";
    }

    @PostMapping("/messages")
    public String addMessage(@Valid MessageForm form, BindingResult result, RedirectAttributes forRedirectModel) {
        if (result.hasErrors()) {
            forRedirectModel.addFlashAttribute("errors", "Твит не бывает пустым");
            return "redirect:/messages";
        }
        messageService.addMessage(form);
        return "redirect:/messages";
    }

    @PostMapping("/messages/{message-id}/delete")
    public String deleteMessage(@PathVariable("message-id") Integer messageId) {
        messageService.deleteMessage(messageId);
        return "redirect:/messages";
    }

    @PostMapping("/messages/{message-id}/update")
    public String update(@PathVariable("message-id") Integer messageId, MessageForm form) {
        form.setId(messageId);
        messageService.update(form);
        return "redirect:/messages";
    }

    @GetMapping("/messages/{message-id}")
    public String getMessageById(Model model, @PathVariable("message-id") Integer messageId) {
        Message message = messageService.getMessage(messageId);
        User user = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        if (!Objects.equals(message.getOwner().getId(), user.getId())) {
            throw new AccessDeniedException("У вас нет прав");
        }
        model.addAttribute("message", message);
        model.addAttribute("user", user);
        return "message";
    }

    @GetMapping("/messages/user/{message-owner-id}")
    public String getMassagesByOwner(Model model, @PathVariable("message-owner-id") Integer userId) {
        User user = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        List<Message> messageByOwnerList = messageService.getMessagesByUser(userId);
        String userName = messageService.getMessagesByUser(userId).get(0).getOwner().getFirst_name();
        model.addAttribute("messageByOwnerList", messageByOwnerList);
        model.addAttribute("userName", userName);
        model.addAttribute("user", user);
        return "user";
    }

}
