package com.twitter.timur.services;

import com.twitter.timur.forms.SignUpForm;

public interface SignUpService {
    public void signUpUser(SignUpForm form);
}
