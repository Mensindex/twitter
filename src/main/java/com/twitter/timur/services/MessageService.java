package com.twitter.timur.services;

import com.twitter.timur.forms.MessageForm;
import com.twitter.timur.models.Message;

import java.util.List;

public interface MessageService {
    void addMessage(MessageForm form);

    List<Message> getAllMessage();

    Message getMessage(Integer messageId);

    void deleteMessage(Integer messageId);

    void update(MessageForm form);

    List<Message> getMessagesByUser(Integer userId);
}
