package com.twitter.timur.services;

import com.twitter.timur.exceptions.MessageNotFoundException;
import com.twitter.timur.forms.MessageForm;
import com.twitter.timur.models.Message;
import com.twitter.timur.repositories.MessageRepository;
import com.twitter.timur.repositories.UserRepository;
import com.twitter.timur.security.details.UserDetailsImpl;
import com.twitter.timur.security.details.UserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;

    @Override
    public void addMessage(MessageForm form) {
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Message message = Message.builder()
                .text(form.getText())
                .owner(userDetails.getUser())
                .build();
        messageRepository.save(message);
    }

    @Override
    public List<Message> getAllMessage() {
        return messageRepository.findAll();
    }

    @Override
    public Message getMessage(Integer messageId) {
        return messageRepository.findById(messageId).orElseThrow(MessageNotFoundException::new);
    }

    @Override
    public void deleteMessage(Integer messageId) {
        messageRepository.deleteById(messageId);
    }

    @Override
    public void update(MessageForm form) {
        Message message = Message.builder()
                .id(form.getId())
                .text(form.getText())
                .owner(messageRepository.getById(form.getId()).getOwner())
                .build();
        messageRepository.save(message);
    }

    @Override
    public List<Message> getMessagesByUser(Integer userId) {
        return messageRepository.findAllByOwner_Id(userId);
    }
}
