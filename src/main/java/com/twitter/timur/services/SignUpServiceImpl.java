package com.twitter.timur.services;

import com.twitter.timur.forms.SignUpForm;
import com.twitter.timur.models.User;
import com.twitter.timur.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Override
    public void signUpUser(SignUpForm form) {
        User user = User.builder()
                .first_name(form.getFirst_name())
                .last_name(form.getLast_name())
                .email(form.getEmail())
                .role(User.Role.USER)
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .build();
        userRepository.save(user);
    }
}
