package com.twitter.timur.repositories;

import com.twitter.timur.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Integer> {
    List<Message> findAllByOwner_Id(Integer id);
}
