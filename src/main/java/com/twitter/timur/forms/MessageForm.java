package com.twitter.timur.forms;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Data
public class MessageForm {
    private Integer id;
    @NotEmpty
    @Length(max=500)
    private String text;
}
