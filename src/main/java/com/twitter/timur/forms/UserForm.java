package com.twitter.timur.forms;

import lombok.Data;

@Data
public class UserForm {
    private Integer id;
    private String first_name;
    private String last_name;
}
