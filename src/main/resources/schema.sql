create table "user"
(
    id           serial primary key,
    first_name   varchar(20),
    last_name    varchar(20),
    email        varchar(255),
    hash_password varchar(255),
    role varchar(255)
);

create table message
(
    id       serial primary key,
    text     varchar,
    owner_id integer,
    foreign key (owner_id) references "user" (id)
);