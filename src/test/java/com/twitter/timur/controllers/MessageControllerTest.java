package com.twitter.timur.controllers;

import com.twitter.timur.controller.MessagesController;
import com.twitter.timur.forms.MessageForm;
import com.twitter.timur.models.Message;
import com.twitter.timur.models.User;
import com.twitter.timur.repositories.MessageRepository;
import com.twitter.timur.repositories.UserRepository;
import com.twitter.timur.security.details.UserDetailsImpl;
import com.twitter.timur.services.MessageService;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

@SpringBootTest
@Transactional
@AutoConfigureEmbeddedDatabase(provider = AutoConfigureEmbeddedDatabase.DatabaseProvider.ZONKY)

public class MessageControllerTest {

    private MessagesController messagesController;
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private MessageService messageService;
    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    public void init (){
        messagesController = new MessagesController(messageService);
        User user = new User();
        UserDetails userDetails = new UserDetailsImpl(user);
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(userDetails,1));
        user.setFirst_name("Тимур");
        user.setLast_name("Гаджиев");
        userRepository.save(user);
    }

    @Test
    public void testAddMessage() {
        MessageForm messageForm = new MessageForm();
        messageForm.setText("Привет!");
        BindingResult bindingResult = new BeanPropertyBindingResult(messageForm, "r");
        RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
        messagesController.addMessage(messageForm, bindingResult, redirectAttributes );
        Message message = messageRepository.findAll().get(0);
        //Проверяем идентичность сообщений
        Assertions.assertEquals(message.getText(), "Привет!");
        //Проверяем идентичность отправителя(owner)
        Assertions.assertEquals(message.getOwner().getFirst_name(), "Тимур");
    }
}
