package com.twitter.timur.controllers;

import com.twitter.timur.controller.SignUpController;
import com.twitter.timur.forms.SignUpForm;
import com.twitter.timur.services.SignUpService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SignUpControllerTest {
    SignUpController signUpController;
    @Mock
    SignUpService signUpService;
    @Captor
    ArgumentCaptor<SignUpForm> argumentCaptor;

    @BeforeEach
    public void init() {
        signUpController = new SignUpController(signUpService);
    }

    @Test
    public void testSignUpUser() {
        SignUpForm signUpForm = SignUpForm.builder()
                .first_name("Тимур")
                .last_name("Гаджиев")
                .email("mensindex@gmail.com")
                .password("1111")
                .build();
        signUpController.signUpUser(signUpForm);
        Mockito.verify(signUpService).signUpUser(argumentCaptor.capture());
        Assertions.assertEquals(argumentCaptor.getValue().getFirst_name(), "Тимур");
        Assertions.assertEquals(argumentCaptor.getValue().getPassword(), "1111");
    }
}
