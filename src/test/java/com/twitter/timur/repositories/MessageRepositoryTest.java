package com.twitter.timur.repositories;

import com.twitter.timur.models.Message;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import javax.transaction.Transactional;
import java.util.List;

@SpringBootTest
@Transactional
@AutoConfigureEmbeddedDatabase(provider = AutoConfigureEmbeddedDatabase.DatabaseProvider.ZONKY)
public class MessageRepositoryTest {
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Test
    public void testFindAllByOwner_Id() {
        jdbcTemplate.update("insert into \"user\" (first_name) values ('Михаил')");
        jdbcTemplate.update("insert into message (text, owner_id) values ('Привет', 1)");
        List<Message> messages = messageRepository.findAllByOwner_Id(1);
        Assertions.assertEquals(messages.get(0).getText(), "Привет");
    }

    @Test
    public void testFindAllByOwner_IdNull() {
        jdbcTemplate.update("insert into \"user\" (first_name) values ('Михаил')");
        jdbcTemplate.update("insert into message (text, owner_id) values ('Привет', 1)");
        List<Message> messages = messageRepository.findAllByOwner_Id(null);
        Assertions.assertTrue(messages.isEmpty());

    }


}
